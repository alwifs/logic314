﻿using System;
using System.Collections.Generic;

namespace Logic10
{
    internal class Program
    {
        static void Main(string[] args)
        {

            
            Console.ReadKey();
        }

        static void simpleArraysum()
        {
            Console.WriteLine("=== SImple Array SUUM===");

            List<int> list = new List<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(10);
            list.Add(11);

            int hasil = simpleArraySum(list);

            Console.WriteLine(hasil);
        }

        static void int simpleArraySum(List<int> ar)
        {
            int result = 0;
            foreach (int element in ar)
            {
                result += element;
            }

            return result;
        }
    }
}

﻿using System;

namespace Logic05
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //contain();
            // padLeft();
            //convertArray();
            Console.ReadKey();
        }

        static void convertArray()
        {
            Console.WriteLine("--- Convert Array ---");
            Console.Write(" Masukkan Angka array {pakai koma}: ");
            int[] array = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            Console.WriteLine(String.Join("\t", array));
        }

        static void padLeft(){
            Console.WriteLine("--- Pad Left ---");
            Console.Write(" Masukkan Kalimat: ");
            int input = int.Parse(Console.ReadLine());

            Console.Write(" Masukkan Panjang Karakter: ");
            int panjang = int.Parse(Console.ReadLine());

            Console.Write(" Masukkan Karakter: ");
            char karakter = char.Parse(Console.ReadLine());

            Console.WriteLine($"Hasil Padleft:  {input.ToString().PadLeft(panjang, karakter)}" );
            Console.WriteLine($"Hasil Padleft:  {input.ToString().PadRight(panjang, karakter)}"); // kebalikan
        }


        static void contain()
        {
            Console.WriteLine("--- Contain ---");
            Console.Write(" Masukkan Kalimat: ");
            string kalimat = Console.ReadLine();

            Console.Write(" Masukkan Contain: ");
            string contain = Console.ReadLine();

            if (kalimat.Contains(contain))
            {
                Console.WriteLine($"Kalimat ({kalimat}) ini Mengandung {contain} ");
            }
            else
            {
                Console.WriteLine($"Kalimat ({kalimat}) ini tidak Mengandung {contain}");
            }
        }
        
    }
}

﻿using System;

namespace Logic01
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Konversi();
            // operatorAritmatika();
            // operatorModulus();
            // oprPenugasan();
            // optPerbandingan();
            // optLogic();
            Penjumlahan();
            Console.ReadKey();
            
        }

        static void Penjumlahan()
        {
            int mangga, apel;

            Console.WriteLine("=== Method Return Type ===");
            Console.Write("Jumlah Mangga: ");
            mangga = Convert.ToInt32(Console.ReadLine());

            Console.Write("Jumlah Apel: ");
            apel = int.Parse(Console.ReadLine());

            int hasil = HitungJumlah(mangga, apel);
            Console.WriteLine($"Hasil Mangga + Apel = {hasil}");
        }
        
        static int HitungJumlah(int mangga, int apel)
        {
            int hasil = 0;
            hasil = mangga + apel;
            return hasil;
        }
        
        static void optLogic()
        {
            Console.Write("Masukkan Umur Anda : ");
            int age = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Password : ");
            string password = Console.ReadLine();

            bool isAdult = age > 18;
            bool isPwdValid = password == "admin";

            if (isAdult && isPwdValid)
            {
                Console.WriteLine("Anda Sudah Dewasa dan Password Valid");
            } else if (isAdult && !isPwdValid)
            {
                Console.WriteLine("Anda Sudah Dewasa dan Password tidak Valid");
            } else if ( !isAdult && isPwdValid)
            {
                Console.WriteLine("Anda Belum Dewasa dan Password Valid");
            }

            else
            {
                Console.WriteLine("Sorry Try Again!");
            }
            

        }
        
        static void optPerbandingan()
        {
            int mangga, apel = 0;

            Console.WriteLine("=== Operator Perbandingan ===");

            Console.Write("Jumlah Mangga = ");
            mangga = int.Parse(Console.ReadLine());
            Console.Write("Jumlah Mangga = ");
            apel = int.Parse(Console.ReadLine());

            Console.WriteLine("=== Hasil Perbandingan adalah ===");
            Console.WriteLine($"Mangga > Apel : {mangga > apel}");
            Console.WriteLine($"Mannga < Apel : {mangga < apel}");
            Console.WriteLine($"Mangga >= Apel : {mangga >= apel}");
            Console.WriteLine($"Mangga <= Apel : {mangga <= apel}");
            Console.WriteLine($"Mangga != Apel : {mangga != apel}");
            Console.WriteLine($"Mangga == Apel : {mangga == apel}");


        }

        static void oprPenugasan()
        {
            
            int mangga = 10;
            int apel = 8;

            mangga = 15;

            Console.WriteLine($"Mangga Awal adalah = {mangga}");
            Console.WriteLine($"Apel Awal adalah = {apel}");

            apel += 7;
            mangga -= apel;

            Console.WriteLine($"Apel sekarang adalah = {apel}");
            Console.WriteLine($"Mangga sekarang adalah = {mangga}");

            apel = 10;
            mangga = 20;
            apel += mangga;
            Console.WriteLine($"Hasilnya adalah = {apel}");

            apel = 20;
            mangga = 10;
            apel -= mangga;
            Console.WriteLine($"Hasilnya adalah = {apel}");

            apel = 20;
            mangga = 10;
            apel *= mangga;
            Console.WriteLine($"Hasilnya adalah = {apel}");

            apel = 20;
            mangga = 10;
            apel /= mangga;
            Console.WriteLine($"Hasilnya adalah = {apel}");

            apel = 20;
            mangga = 3;
            apel %= mangga;
            Console.WriteLine($"Hasilnya adalah = {apel}");
        }
        
        
        static void operatorModulus()
        {
            int mangga, apel, hasil = 0;

            Console.Write("Jumlah Mangga: ");
            mangga = Convert.ToInt32(Console.ReadLine());

            Console.Write("Jumlah Apel: ");
            apel = int.Parse(Console.ReadLine());

            hasil = mangga % apel;

            Console.WriteLine($"Hasil Modulus Mangga % Apple adalah = {hasil}");
        }

        static void operatorAritmatika()
        {
            int mangga, apel, hasil = 0;

            Console.Write("Jumlah Mangga: ");
            mangga = Convert.ToInt32(Console.ReadLine());

            Console.Write("Jumlah Apel: ");
            apel = int.Parse(Console.ReadLine());

            hasil = mangga + apel;

            Console.WriteLine( $"Hasil Mangga + Apel = {hasil}");


        }
        static void Konversi()
        {
            int umur = 30;
            string strumur = umur.ToString();
            Console.WriteLine("Umur kamu adalah: ", strumur);

            int myInt = 10;
            double myDouble = 5.23;
            bool myBool = false;

            string myString = Convert.ToString(myInt);
            double myConvDouble = Convert.ToDouble(myInt);
            int myConvInt = Convert.ToInt32(myDouble);
            string myConvBool = Convert.ToString(myBool);

            Console.WriteLine(myString);
            Console.WriteLine(myConvDouble);
            Console.WriteLine(myConvInt);
            Console.WriteLine(myConvBool);
            
             Console.ReadKey();
        }
    }
}

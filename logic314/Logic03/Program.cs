﻿using System;
using System.Drawing;
using System.Linq;
using System.Threading;

namespace Logic03
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //tugasC();
            //tugasD1();
            //tugasD2();
            //tugasE();
            //tugasF();
            //loopingWhile();
            //doWhile();
            //loopingFor();
            //incrementFor();
            //breakFor();
            //continueFor();
            //soal01();
            //soal02();
            //soal03();
            //soal04();
            //soal05();
            //soal06();
            //soal07();
            //soal08();
            //forBersarang();
            //tipedataArray();
            //arrayForeach();
            //arrayFor();
            //Console.ReadKey();
            //array2Dimensi();
            //array2Dfor();
            //splitJoin();
            //subbString();
            //subbString();
            //stringToCharArray();
        }

        static void stringToCharArray()
        {
            Console.WriteLine("--- String to Char Array ---");

            Console.Write("Masukkan Kalimat Kamu: ");
            string kalimat = Console.ReadLine();

            char[] charArray = kalimat.ToCharArray();
            
            foreach (char ch in charArray)
            {
                Console.WriteLine(ch);
            }
        }

        static void subbString()
        {
            Console.WriteLine("--- SubString ---");

            Console.Write("Masukkan Kalimat Kamu: ");
            string kalimat = Console.ReadLine();

            Console.WriteLine("Substring (1, 4): " + kalimat.Substring(0, 6));
            Console.WriteLine("Substring (5, 2): " + kalimat.Substring(5, 3));
            Console.WriteLine("Substring (7, 2): " + kalimat.Substring(7, 9));
            Console.WriteLine("Substring (9): " + kalimat.Substring(9));
        }
        static void splitJoin()
        {
            Console.WriteLine("--- Split Join ---");

            Console.Write("Masukkan Kalimat Kamu: ");
            string kalimat = Console.ReadLine();

            
            string[] kataArray = kalimat.Split(" ");
            foreach (string kata in kataArray)
            {
                Console.WriteLine(kata);
            }

            Console.WriteLine(string.Join("+", kataArray));
            Console.WriteLine(kataArray);
        }

        static void array2Dfor()
        {
            int[,] array2dfor = new int[,]
            {
                { 1, 2, 3 },
                { 4, 5, 6 },
                { 7, 8, 9 }
            };
            for (int k=0; k<3; k++)
            {
                for (int j=0; j<3; j++)
                {
                    Console.Write(array2dfor[k, j] + " ");
                }
                Console.WriteLine();
            }
        }

        static void array2Dimensi()
        {
            int[, ] array2d = new int[,]
            {
                { 1, 2, 3 },
                { 4, 5, 6 },
                { 7, 8, 9 }
            };
            Console.WriteLine(array2d[0, 2]);
        }

        static void arrayFor()
        {
            string[] array = new string[]
            {
                "Alwi Fadli Siregar",
                "Abdullah Muafa",
                "Ilham Rizky",
                "Marcelino",
                "Indigo indihome"
            };

            for ( int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i]);
            }
        }

        static void arrayForeach()
        {
            string[] array = new string[]
            {
                "Alwi Fadli Siregar",
                "Abdullah Muafa",
                "Ilham Rizky",
                "Marcelino",
                "Indigo indihome"
            };

            foreach (string item in array)
            {
                Console.WriteLine(item);
            }
        }

        static void tipedataArray()
        {
            int[] staticIntArray = new int[4];

            staticIntArray[0] = 1;
            staticIntArray[1] = 2;
            staticIntArray[2] = 3;
            staticIntArray[3] = 4;

            Console.WriteLine("--- Array Static ---");
            Console.WriteLine(staticIntArray[0]);
            Console.WriteLine(staticIntArray[1]);
            Console.WriteLine(staticIntArray[2]);
            Console.WriteLine(staticIntArray[3]);
        }
        static void forBersarang()
        {
            for (int i=0; i<3; i++)
            {
                for (int j=4; j<7; j++)
                {
                    Console.Write($"[{i},{j}]");
                }
                Console.Write("\n");
            }
        }
        static void soal01()
        {
            int nilaiMhs;

            Console.Write("Masukkan Nilai Mahasiswa : ");
            nilaiMhs = int.Parse(Console.ReadLine());

            if (nilaiMhs >= 90)
            {
                Console.WriteLine($"Kamu mendapatkan Nilai A dengan Nilai {nilaiMhs}");

            }
            else if (nilaiMhs >= 70)
            {
                Console.WriteLine($"Kamu mendapatkan Nilai B dengan Nilai {nilaiMhs}");
            }
            else if (nilaiMhs >= 50)
            {
                Console.WriteLine($"Kamu mendapatkan Nilai C dengan Nilai {nilaiMhs}");
            }
            else
            {
                Console.WriteLine($"Kamu mendapatkan Nilai D dengan Nilai {nilaiMhs}");
            }
        }

        static void soal02()
        {
            Console.Write("Masukkan Jumlah Pulsa: ");
            int pulsa = int.Parse(Console.ReadLine());

            int pulsa10 = 10000;
            int pulsa25 = 25000;
            int pulsa50 = 50000;
            int pulsa100 = 100000;

            int point10 = 80;
            int point25 = 200;
            int point50 = 400;
            int point100 = 800;

            if (pulsa == pulsa10)
            {
                Console.WriteLine($"Selamat Kamu mendapatkan Reward Poin {point10} karena telah mengisi Pulsa {pulsa} ");
            } else if (pulsa == pulsa25)
            {
                Console.WriteLine($"Selamat Kamu mendapatkan Reward Poin {point25} karena telah mengisi Pulsa {pulsa} ");
            }
            else if (pulsa == pulsa50)
            {
                Console.WriteLine($"Selamat Kamu mendapatkan Reward Poin {point50} karena telah mengisi Pulsa {pulsa} ");
            }
            else if (pulsa == pulsa100)
            {
                Console.WriteLine($"Selamat Kamu mendapatkan Reward Poin {point100} karena telah mengisi Pulsa {pulsa} ");
            }
            else
            {
                Console.WriteLine("Masukkan Nominal Pulsa yang Benar ");
            }

        }

        static void soal03()
        {
            Console.Write("Masukkan Jumlah Belanjaan: ");
            int belanja = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Jauh Jarak: ");
            int jarak = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Kode Promo Kamu: ");
            string kodePromo = Console.ReadLine().ToLower();

            double diskonVoucher;
            double ongkosKirim;
            double totalBelanja;
            diskonVoucher = 0.40 * belanja;
            if (belanja >= 30000 && kodePromo == "jktovo")
            {
                if (diskonVoucher > 30000)
                {
                    diskonVoucher = 30000;
                    if (jarak > 5)
                    {
                        ongkosKirim = 1000 * jarak;
                        totalBelanja = belanja - diskonVoucher + ongkosKirim;
                        Console.WriteLine("Belanja : " + belanja + "\n Diskon (40%) : " 
                            + diskonVoucher +" \n Ongkir : " + ongkosKirim + "\n Total belanja: " + totalBelanja);
                    }
                    else
                    {
                        ongkosKirim = 5000;
                        totalBelanja = belanja - diskonVoucher + ongkosKirim;
                        Console.WriteLine("Belanja : " + belanja + "\n Diskon (40%) : "
                            + diskonVoucher + " \n Ongkir : " + ongkosKirim + "\n Total belanja: " + totalBelanja);
                    }
                }
                else
                {
                    if (jarak > 5)
                    {
                        ongkosKirim = 1000 * jarak;
                        totalBelanja = belanja - diskonVoucher + ongkosKirim;
                        Console.WriteLine("Belanja : " + belanja + "\n Diskon (40%) : "
                            + diskonVoucher + " \n Ongkir : " + ongkosKirim + "\n Total belanja: " + totalBelanja);
                    }
                    else
                    {
                        ongkosKirim = 5000;
                        totalBelanja = belanja - diskonVoucher + ongkosKirim;
                        Console.WriteLine("Belanja : " + belanja + "\n Diskon (40%) : "
                            + diskonVoucher + " \n Ongkir : " + ongkosKirim + "\n Total belanja: " + totalBelanja);
                    }
                }
            }
            else
            {
                diskonVoucher = 0;
                if (jarak > 5)
                {
                    ongkosKirim = 1000 * jarak;
                    totalBelanja = belanja - diskonVoucher + ongkosKirim;
                    Console.WriteLine("Belanja : " + belanja + "\n Diskon (40%) : "
                            + diskonVoucher + " \n Ongkir : " + ongkosKirim + "\n Total belanja: " + totalBelanja);
                }
                else
                {
                    ongkosKirim = 5000;
                    totalBelanja = belanja - diskonVoucher + ongkosKirim;
                    Console.WriteLine("Belanja : " + belanja + "\n Diskon (40%) : "
                            + diskonVoucher + " \n Ongkir : " + ongkosKirim + "\n Total belanja: " + totalBelanja);
                }

            }


            /*
            int jarakPromo = 5;
            int maxPromo = 30000;
            int jarak1km = 1000;

            if (belanja >= maxPromo)
            {
                Console.WriteLine($"Kamu berhasil mendapatkan Promo 40% sebesar {JKTOVO} dengan total belanja: " + (belanja-JKTOVO));
                if (jarak == jarakPromo)
                {
                    Console.WriteLine($" Kamu Mendapatkan Diskon Ongkir {jarakPromo}KM senilai 5000");
                } else {
                    Console.WriteLine($" Kamu Mendapatkan Diskon Ongkir {jarakPromo}KM + diluar Ongkir " + ((jarak - jarakPromo) * jarak1km));
                }

            } else if ( belanja < maxPromo)
            {
                Console.WriteLine($"Maaf Kamu tidak mendapatkan Promo apapun, dengan total belanjaan {belanja} + {jarak}: " + (belanja + (jarak * jarak1km)));
            }
            */

        }

        static void soal04()
        {
            Console.WriteLine("Soal no 4");
            Console.Write("Masukkan Total Belanja Anda: ");
            int belanja = int.Parse(Console.ReadLine());

            Console.Write("Biaya Ongkos kirim : ");
            int ongkosKirim = int.Parse(Console.ReadLine());

            Console.Write("Silahkan Pilih Voucher Anda 1, 2 & 3 : ");
            int vocherBelanja = int.Parse(Console.ReadLine());

            int diskonOngkir;
            int diskonBelanja;
            int totalBelanja;
            totalBelanja =belanja + ongkosKirim;

            if (vocherBelanja == 1 && belanja >= 30000)
            {
                diskonBelanja = 5000;
                diskonOngkir = 5000;
                Console.WriteLine(" Belanjaan Kamu " + belanja + "\n Ongkos Kirim Kamu adalah: " 
                    + ongkosKirim + "\n Kamu mendapatkan Diskon Belanja: "+ diskonBelanja + "\n Kamu mendapatkan Diskon Ongkir: " 
                    + diskonOngkir + "\n Total belanjaan Kamu adalah: " + (totalBelanja - (diskonOngkir + diskonBelanja)));
                
                
            }
            else if (vocherBelanja == 1 && belanja >= 50000)
            {
                diskonBelanja = 5000;
                diskonOngkir = 5000;
                Console.WriteLine(" Belanjaan Kamu " + belanja + "\n Ongkos Kirim Kamu adalah: "
                    + ongkosKirim + "\n Kamu mendapatkan Diskon Belanja: " + diskonBelanja + "\n Kamu mendapatkan Diskon Ongkir: "
                    + diskonOngkir + "\n Total belanjaan Kamu adalah: " + (totalBelanja - (diskonOngkir + diskonBelanja)));

            }
            
            else if (vocherBelanja == 2 && belanja >= 50000)
            {
                diskonBelanja = 10000;
                diskonOngkir = 10000;
                Console.WriteLine(" Belanjaan Kamu " + belanja + "\n Ongkos Kirim Kamu adalah: "
                    + ongkosKirim + "\n Kamu mendapatkan Diskon Belanja: " + diskonBelanja + "\n Kamu mendapatkan Diskon Ongkir: "
                    + diskonOngkir + "\n Total belanjaan Kamu adalah: " + (totalBelanja - (diskonOngkir + diskonBelanja)));
               
            }
            else if (vocherBelanja == 3 && belanja >= 100000)
            {
                diskonBelanja = 10000;
                diskonOngkir = 20000;
                Console.WriteLine(" Belanjaan Kamu " + belanja + "\n Ongkos Kirim Kamu adalah: "
                    + ongkosKirim + "\n Kamu mendapatkan Diskon Belanja: " + diskonBelanja + "\n Kamu mendapatkan Diskon Ongkir: "
                    + diskonOngkir + "\n Total belanjaan Kamu adalah: " + (totalBelanja - (diskonOngkir + diskonBelanja)));
                
            }

            else if (vocherBelanja == 1 && belanja >= 100000)
            {
                diskonBelanja = 50000;
                diskonOngkir = 5000;
                Console.WriteLine(" Belanjaan Kamu " + belanja + "\n Ongkos Kirim Kamu adalah: "
                    + ongkosKirim + "\n Kamu mendapatkan Diskon Belanja: " + diskonBelanja + "\n Kamu mendapatkan Diskon Ongkir: "
                    + diskonOngkir + "\n Total belanjaan Kamu adalah: " + (totalBelanja - (diskonOngkir + diskonBelanja)));

            }

            else if (vocherBelanja == 2 && belanja >= 100000)
            {
                diskonBelanja = 50000;
                diskonOngkir = 5000;
                Console.WriteLine(" Belanjaan Kamu " + belanja + "\n Ongkos Kirim Kamu adalah: "
                    + ongkosKirim + "\n Kamu mendapatkan Diskon Belanja: " + diskonBelanja + "\n Kamu mendapatkan Diskon Ongkir: "
                    + diskonOngkir + "\n Total belanjaan Kamu adalah: " + (totalBelanja - (diskonOngkir + diskonBelanja)));

            }

            else
            {
               
                Console.WriteLine("Maaaf, Kamu tidak mendapatkan Voucer Diskon Belanja dan Potongan Ongkos Kirim ");
                Console.WriteLine("\n Belanjaan Kamu " + belanja + "\n Ongkos Kirim Kamu adalah: "
                    + ongkosKirim + "\n Kamu tidak mendapatkan Voucher diskon dan Potongan Ongkir  " + "\n Total belanjaan Kamu adalah: " + totalBelanja);
            }
        }



        static void soal05()
        {
            Console.WriteLine("--- Soal 05 ---");
            Console.Write("Masukkan Nama Kamu : ");
            string nama = Console.ReadLine().ToLower();

            Console.Write("Masukkan Tahun Kelahiran Kamu : ");
            int tahun = int.Parse(Console.ReadLine());

            if (tahun >= 1995)
            {
                Console.WriteLine($"Nama Kamu {nama} Berdasarkan Tahun Lahir, Kamu tergolong  Generasi Z");
            }
            else if (tahun >= 1980)
            {
                Console.WriteLine($"Nama Kamu {nama} Berdasarkan Tahun Lahir, Kamu tergolong  Generasi Y");
            }
            else if (tahun >= 1965)
            {
                Console.WriteLine($"Nama Kamu {nama} Berdasarkan Tahun Lahir, Kamu tergolong  Generasi X");
            }
            else if (tahun >= 1944)
            {
                Console.WriteLine($"Nama Kamu {nama} Berdasarkan Tahun Lahir, Kamu tergolong  Generasi Baby Boomer");
            }
            else
            {
                Console.WriteLine("Masukkan Umur yang benar, mulai dari 1944");
            }
        }

        static void soal06()
        {
            Console.WriteLine("--- Soal 06 ---");
            Console.Write("Masukkan Nama Kamu : ");
            string nama = Console.ReadLine();

            Console.Write("Masukkan Tunjangan Kamu : ");
            double tunjangan = double.Parse(Console.ReadLine());

            Console.Write("Masukkan Gaji Pokok Kamu : ");
            double gajiPokok = double.Parse(Console.ReadLine());

            Console.Write("Masukkan Banyak Bulan : ");
            double banyakBulan = double.Parse(Console.ReadLine());

            
            double gajiBulanan = gajiPokok + tunjangan;
            double persenBpjs = 0.03;
            double bpjsGaji = gajiBulanan * persenBpjs;
            double potongan05 = 0.05;
            double potongan10 = 0.1;
            double potongan15 = 0.15;





            if (gajiBulanan == 0)
            {
                Console.WriteLine("Masukkan Gaji Yang benar, Gaji Pokok atau Tunjangan tidak boleh Kosong");
            }
            else if (gajiBulanan < 5000000)
            {
                Console.WriteLine("Pajak Gaji kamu adalah: " + (gajiBulanan * 0.05)
                    + "\n BPJS Kamu adalah: " + bpjsGaji + "\n Gaji bersih Bulanan Kamu adalah: "
                    + (gajiBulanan - (gajiBulanan * (potongan05 + persenBpjs))) + $"\n Gaji total Kamu selama {banyakBulan} adalah "
                    + ((gajiBulanan - (gajiBulanan * (potongan05 + persenBpjs))) * banyakBulan));
            }
            else if (gajiBulanan > 5000000)
            {

                Console.WriteLine("Pajak Gaji kamu adalah: " + (gajiBulanan * 0.1)
                    + "\n BPJS Kamu adalah: " + bpjsGaji + "\n Gaji bersih Bulanan Kamu adalah: "
                    + (gajiBulanan - (gajiBulanan * (potongan10 + persenBpjs))) + $"\n Gaji total Kamu selama {banyakBulan} adalah "
                    + ((gajiBulanan - (gajiBulanan * (potongan10 + persenBpjs))) * banyakBulan));
            }
            else if (gajiBulanan > 10000000)
            {
                Console.WriteLine("Pajak Gaji kamu adalah: " + (gajiBulanan * 0.15)
                    + "\n BPJS Kamu adalah: " + bpjsGaji + "\n Gaji bersih Bulanan Kamu adalah: "
                    + (gajiBulanan - (gajiBulanan * (potongan15 + persenBpjs))) + $"\n Gaji total Kamu selama {banyakBulan} adalah "
                    + ((gajiBulanan - (gajiBulanan * (potongan15 + persenBpjs))) * banyakBulan));
            }

            else {
                Console.WriteLine("Masukkan Gaji Yang benar");
            }

            
            /*
            Console.WriteLine("BPJS Bulanan Kamu adalah: " + bpjsGaji);
            Console.WriteLine("Gaji Bulanan Kamu adalah: " + gajiBulanan);
            Console.WriteLine($"Total Gaji kamu sebanyak {banyakBulan} Kamu adalah: " + (banyakBulan*gajiBulanan));
            */
        }

        static void soal07()
        {
            Console.WriteLine("--- Soal 07 ---");
            Console.Write("Masukkan Nama Kamu : ");
            string nama = Console.ReadLine().ToLower();

            Console.Write("Masukkan Berat Badan Kamu : ");
            double beratBadan = double.Parse(Console.ReadLine());

            Console.Write("Masukkan Berat Tinggi Kamu : ");
            double tinggiBadan = double.Parse(Console.ReadLine());

            double meter = tinggiBadan / 100;
            double nilaiBMI = (beratBadan / (meter*meter));

            Console.WriteLine(nilaiBMI);
            
            if (nilaiBMI >= 25)
            {
                Console.WriteLine("Nilai Indeks BMI Kamu Gemuk");
            }
            else if (nilaiBMI <= 25)
            {
                Console.WriteLine("Nilai Indeks BMI Kamu Langsing");
            }
            else if (nilaiBMI <= 18.5)
            {
                Console.WriteLine("Nilai Indeks BMI Kamu Terlalu Kurus");
            }
            else
            {
                Console.WriteLine("Masukkan Nilai BMI yang Benar");
            }

        }

        static void soal08()
        {
            Console.WriteLine("--- Soal 08 ---");
            Console.Write("Masukkan Nama Kamu : ");
            string nama = Console.ReadLine().ToLower();

            Console.Write("Masukkan Nilai Matematika Kamu : ");
            int nilaiMTK = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Nilai Fisika Kamu : ");
            int nilaiFisika = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Nilai Kimia Kamu : ");
            int nilaiKimia = int.Parse(Console.ReadLine());

            int nilaiTotal = ( (nilaiMTK + nilaiFisika + nilaiKimia) / 3 );
            int nilaiMax = 100;
             

            if (nilaiTotal >= 75 && nilaiTotal <= nilaiMax)
            {
                Console.WriteLine($"Selamat {nama} Nilai Rata-rata kamu {nilaiTotal}, Selamat Kamu Berhasil, Kamu Hebat");
            }
            else if (nilaiTotal <= 75)
            {
                Console.WriteLine($"Maaf Kamu Gagal");
            }
        }
        
       
        static void continueFor()
        {
            for (int i = 0; i < 10; i++)
            {
                if (i == 4)
                {
                    continue;
                }
                Console.WriteLine(i);
            }
        }
        
        static void breakFor()
        {
            for (int i=0; i<10; i++)
            {
                if(i == 4)
                {
                    break;
                }
                Console.WriteLine(i);
            }
        }

        static void incrementFor()
        {
            for (int i = 10; i > 0; i--)
            {
                Console.WriteLine(i);
            }
        }

        static void loopingFor()
        {
            for (int i=0; i<10; i++)
            {
                Console.WriteLine(i);
            }
        }
        static void doWhile()
        {
            int a = 0;
            do
            {
                Console.WriteLine(a);
                a++;
            } while (a <= 100);
        }

        static void loopingWhile()
        {
            Console.WriteLine("--- Perulangan While ---");
            bool ulangi = true;
            int nilai = 1;

            while (ulangi)
            {
                Console.WriteLine($"Proses ke {nilai}");
                nilai++;

                Console.WriteLine("ulangi Proses? (y/n)");
                string input = Console.ReadLine();
                if (input.ToLower() == "y")
                {
                    ulangi = true;

                }
                else if (input.ToLower() == "n")
                {
                    ulangi = false;
                }
                else
                {
                    nilai = 1;
                    Console.WriteLine("Masukkan hanya huruf y dan n");
                    ulangi = true;
                }
            }
        }

        static void tugasC()
        {
            Console.Write("Masukkan Angka: ");
            int angka = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Angka Pembagi: ");
            int pembagi = int.Parse(Console.ReadLine());

            int hasilBagi = angka % pembagi;

            if (angka % pembagi == 0)
            {
                Console.WriteLine($"Hasil Modulus {angka} % {pembagi} adalah: 0");
            }
            else
            {
                Console.WriteLine($"Hasil Modulus {angka} % {pembagi} adalah: " + hasilBagi);
            }

            //Console.WriteLine($"Hasil Modulus {angka} + {pembagi} adalah: " + hasilBagi);
         }
    
        static void tugasD1()
        {
            int ikatRokok;
            int jumlahRokok;
            int nilaiJual;
            
            

            Console.Write("Masukkan Jumlah Rokok : ");
            jumlahRokok = int.Parse(Console.ReadLine());

            ikatRokok = jumlahRokok % 8;
            nilaiJual = (jumlahRokok / 8) * 500;

            //int nilaiBagi = jumlahRokok / ikatRokok;
            //int nilaiMod = jumlahRokok % ikatRokok;
            //int hasilJual = nilaiMod * nilaiJual;

            //Console.WriteLine($"Batang Rokok yang berhasil dirangkai sebanyak {nilaiBagi}" + $" Sisa Puntung Rokok ialah {nilaiMod}");
            // Console.WriteLine($"Harga Rokok yang Laku dari {nilaiMod} ialah " + hasilJual);

            if (jumlahRokok % 8 >= 0)
            {
                
                Console.WriteLine("1. Hasil dari Ikat Rokok adalah "+(jumlahRokok/8)+" Sisa Ikat Rokok Adalah "+ikatRokok);
                Console.WriteLine("2. Hasil Jual dari Ikat rokok ialah "+nilaiJual);
            }
            else
            {
                Console.WriteLine("Nilai Rokok adalah ");
            }


            /*
            if (puntungRokok != ikatRokok)
            {
                Console.WriteLine("Jumlah Total Rokok adalah " + (nilaiJumlah * ikatRokok) % ikatRokok);
            }
            if (puntungRokok != ikatRokok)
            {
                Console.WriteLine("");
            

            if (nilaiJumlah % ikatRokok == 0)
            {
                Console.WriteLine("Jumlah Puntung Rokok adalah");
            } else
            {

            }
            */




        }
        /*
        static void tugasD2()
        {
            int puntungRokok = 1;
            int ikatRokok = 8;
            int nilaiJumlah = 20;
            int nilaiJual = 500;
            //int totalRokok = (ikatRokok * nilaiJumlah) / puntungRokok;

            if (puntungRokok != ikatRokok)
            {
                Console.WriteLine("Jumlah Total Harga Jual Rokok adalah ");
            }
            if (puntungRokok != ikatRokok)
            {
                Console.WriteLine("");
            }

        }
        */
        static void tugasE()
        {
            int nilai;

            Console.Write("Masukkan Nilai Kamu : ");
            nilai = int.Parse(Console.ReadLine());

            if (nilai >= 80)
            {
                Console.WriteLine($"Kamu mendapatkan Nilai A dengan Nilai {nilai}");

            }
            else if (nilai >= 60)
            {
                Console.WriteLine($"Kamu mendapatkan Nilai B dengan Nilai {nilai}");
            }
            else
            {
                Console.WriteLine($"Kamu mendapatkan Nilai C dengan Nilai {nilai}");
            }

        }
        static void tugasF()
        {
            Console.Write("Masukkan Angka: ");
            int angka = int.Parse(Console.ReadLine());

            if (angka % 2 == 0)
            {
                Console.WriteLine($"Jenis dari {angka} adalah Nilai Genap ");
            }
            else
            {
                Console.WriteLine($"Jenis dari {angka} adalah Nilai Ganjil ");
            }
        }

    }
}      

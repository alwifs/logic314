﻿using System;
using System.Collections.Generic;

namespace Logic04_PR
{
    internal class Program
    {
        static void Main(string[] args)
        {

            //tugasDay401();
            //tugasDay402();
            //tugasDay403();
            //tugasDay404();
            //tugasDay405();
            //tugasDay406();
            tugasDay407();
            //tugasDay408();
            //tugasDay409();
            //tugasDay410();
            //tugasDay411();
            //tugasDay412();
            Console.ReadKey();
        }
        static void tugasDay401()
        {
            Console.WriteLine("--- Soal No 01 ---");
            Console.Write("Masukkan Golongan Pekerjaan Mu: ");
            double golonganKerja = double.Parse(Console.ReadLine());

            Console.Write("Masukkan Jumlah Kerja Kamu: ");
            double jamKerja = double.Parse(Console.ReadLine());

            double golonganSatu = 2000;
            double golonganDua = 3000;
            double golonganTiga = 4000;
            double golonganEmpat = 5000;
            double lembur = 0.5;


            if (golonganKerja == 1) {
                if (jamKerja <= 40) 
                {
                    Console.WriteLine($"Upah Kerja Kamu bekerja selama {jamKerja} Jam adalah " + jamKerja * golonganSatu);
                    Console.WriteLine($"Upah Lembur Kamu adalah " + (lembur * 0));
                    Console.WriteLine($"Total Upah yang Kamu dapat adalah " + jamKerja * golonganSatu);
                }
                
                else if (jamKerja > 40)
                {
                    double totalLembur = ((jamKerja - 40) * (lembur * golonganSatu));
                    Console.WriteLine($"Upah Kerja Kamu bekerja selama {jamKerja} Jam adalah " + ((40 * golonganSatu) + totalLembur));
                    Console.WriteLine($"Upah Lembur Kamu adalah " + totalLembur);
                    Console.WriteLine($"Total Upah yang Kamu dapat adalah " + ((40 * golonganSatu) + totalLembur));
                } else
                {
                    Console.WriteLine("Masukkan Jam kerja yang Bener ");
                }
            }
            else if (golonganKerja == 2)
            {
                if (jamKerja <= 40)
                {
                    Console.WriteLine($"Upah Kerja Kamu bekerja selama {jamKerja} Jam adalah " + jamKerja * golonganDua);
                    Console.WriteLine($"Upah Lembur Kamu adalah " + (lembur * 0));
                    Console.WriteLine($"Total Upah yang Kamu dapat adalah " + jamKerja * golonganDua);
                }

                else if (jamKerja > 40)
                {
                    double totalLembur = ((jamKerja - 40) * (lembur * golonganDua));
                    Console.WriteLine($"Upah Kerja Kamu bekerja selama {jamKerja} Jam adalah " + ((40 * golonganDua) + totalLembur));
                    Console.WriteLine($"Upah Lembur Kamu adalah " + totalLembur);
                    Console.WriteLine($"Total Upah yang Kamu dapat adalah " + ((40 * golonganDua) + totalLembur));
                }
                else
                {
                    Console.WriteLine("Masukkan Jam kerja yang Bener ");
                }
            }
         else if (golonganKerja == 3)
            {
                if (jamKerja <= 40)
                {
                    Console.WriteLine($"Upah Kerja Kamu bekerja selama {jamKerja} Jam adalah " + jamKerja * golonganTiga);
                    Console.WriteLine($"Upah Lembur Kamu adalah " + (lembur * 0));
                    Console.WriteLine($"Total Upah yang Kamu dapat adalah " + jamKerja * golonganTiga);
                }

                else if (jamKerja > 40)
                {
                    double totalLembur = ((jamKerja - 40) * (lembur * golonganTiga));
                    Console.WriteLine($"Upah Kerja Kamu bekerja selama {jamKerja} Jam adalah " + ((40 * golonganTiga) + totalLembur));
                    Console.WriteLine($"Upah Lembur Kamu adalah " + totalLembur);
                    Console.WriteLine($"Total Upah yang Kamu dapat adalah " + ((40 * golonganTiga) + totalLembur));
                }
                else
                {
                    Console.WriteLine("Masukkan Jam kerja yang Bener ");
                }
            }
            else if (golonganKerja == 4)
            {
                if (jamKerja <= 40)
                {
                    Console.WriteLine($"Upah Kerja Kamu bekerja selama {jamKerja} Jam adalah " + jamKerja * golonganEmpat);
                    Console.WriteLine($"Upah Lembur Kamu adalah " + (lembur * 0));
                    Console.WriteLine($"Total Upah yang Kamu dapat adalah " + jamKerja * golonganEmpat);
                }

                else if (jamKerja >= 40)
                {
                    double totalLembur = ((jamKerja - 40) * (lembur * golonganEmpat));
                    Console.WriteLine($"Upah Kerja Kamu bekerja selama {jamKerja} Jam adalah " + ((40 * golonganEmpat) + totalLembur));
                    Console.WriteLine($"Upah Lembur Kamu adalah " + totalLembur);
                    Console.WriteLine($"Total Upah yang Kamu dapat adalah " + ((40 * golonganEmpat) + totalLembur));
                }
                else
                {
                    Console.WriteLine("Masukkan Jam kerja yang Bener ");
                }
            }
            else
            {
                Console.WriteLine("Masukkan Golongan dan Jam Kerja yang Bener");
            }

        }

        static void tugasDay402()
        {
            Console.WriteLine(" --- Soal 02 --- ");
            //Console.WriteLine("Masukkan Kalimat: ");
            //string kalimat = Console.ReadLine();

            //Console.WriteLine("Kata 1: " + kalimat.Substring(0, 6));
            //Console.WriteLine("Substring (5, 2): " + kalimat.Substring(5, 3));
            //Console.WriteLine("Substring (7, 2): " + kalimat.Substring(7, 9));
            //Console.WriteLine("Substring (9): " + kalimat.Substring(9));


            Console.Write("Masukkan Kalimat Kamu: ");
            string kalimat = Console.ReadLine();

            int no = 1;
            string[] kataArray = kalimat.Split(" ");
            foreach (string kata in kataArray)
            {
                Console.WriteLine($"Kata {no} "+kata);
                no++;
            }

            Console.WriteLine("Jumlah Kata: " + kataArray.Length);
        }

        static void tugasDay403()
        {
            Console.WriteLine(" --- Soal 03 --- ");
            Console.Write("Masukkan Kalimat Kamu: ");
            string kalimat = Console.ReadLine();

            string[] kataArray = kalimat.Split('k', 'a', 'y', 'n', 'm');

            foreach (string kata in kataArray)
            {
               
            }

            Console.WriteLine(string.Join("*", kataArray));
            Console.WriteLine(kataArray);

        }

        static void tugasDay404()
        {
            Console.WriteLine(" --- Soal 04 --- ");
            Console.Write("Masukkan Kalimat Kamu: ");
            string kalimat = Console.ReadLine();

            string[] kataArray = kalimat.Split('A', 'u', 'M', 'u', 'n');

            foreach (string kata in kataArray)
            {

            }

            Console.WriteLine(string.Join("*", kataArray));
            Console.WriteLine(kataArray);

        }

        static void tugasDay405()
        {
            Console.WriteLine("--- SubString ---");

            Console.Write("Masukkan Kalimat Kamu: ");
            string kalimat = Console.ReadLine();

            Console.Write($"Hasil dari Kalimat {kalimat} adalah: " + kalimat.Substring(1, 3) + " " + kalimat.Substring(7, 3)+ " " + kalimat.Substring(12, 3));
            
        }

        static void tugasDay406()
        {
            int i, panjang;

            Console.Write("Masukkan Panjang Angka: ");
            panjang = int.Parse(Console.ReadLine());


            for (i = 1; i <= panjang; ++i)
            {
                if (i % 2 == 0)
                {
                    Console.Write(i * 5 + " ");
                }
                else
                {
                    Console.Write("-" + i * 5 + " ");
                }



            }
        }

        static void tugasDay407()
        {
            int i, panjang;

            Console.Write("Masukkan Panjang Angka: ");
            panjang = int.Parse(Console.ReadLine());
            

            for (i= 0; i <= panjang; ++i)
            {
                Console.Write(i * 3 + " ");
                
                //if (i % 9 == 0)
                //{

                //        Console.Write("*" + " ");
                //    }
                //else
                //    {
                //        Console.Write(i*3+ " ");
                //    }



            }

        }

        static void tugasDay408()
        {
            int n1 = 1, n2 = 1, n3, i;

            Console.Write("Masukkan Panjang Angka: ");
            int panjang = int.Parse(Console.ReadLine());

            Console.Write(n1 + " " + n2 + " ");
            for (i = 2; i < panjang; ++i)
            {
                n3 = n1 + n2;
                Console.Write(n3 + " ");
                n1 = n2;
                n2 = n3; 
            }
        }

        static void tugasDay409()
        {
            Console.WriteLine("--- Soal No 09 ---");
            Console.Write("Masukkan Jam: ");
            string waktu = Console.ReadLine();

            Console.Write("Masukkan Format Waktu: ");
            string format = Console.ReadLine().ToUpper();

            Console.WriteLine("Waktu yang Kamu Masukkan Adalah: "+ waktu+format);
            if ( format == "PM")
            {
                int jamBaru = int.Parse(waktu.Substring(0, 2));
                jamBaru += 12;
                Console.WriteLine(jamBaru + waktu.Substring(2));

            }
            else if (format == "AM")
            {
                Console.WriteLine(waktu);
            }
            else
            {
                Console.WriteLine("Silahkan Masukkan Format Waktu AM/ PM");
            }


            //Console.WriteLine(waktu);
            // Console.WriteLine(format);

            //Console.WriteLine("Substring (9): " + waktu.Substring(9));

        }

        static void tugasDay410()
        {
            Console.WriteLine("--- Soal No 10 ---");
            Console.Write("Masukkan Kode Baju 1, 2, 3: ");
            int kodeBaju = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Ukuran Baju S, M, Lainnya: ");
            string ukuranBaju = Console.ReadLine().ToLower();

            int ukuranS = 0;
            int ukuranM = 0;
            int ukuranLain = 0;

            
            string iMP = "IMP";
            string prada = "Prada";
            string gucci = "Gucci";

            if (kodeBaju == 1)
            {
                if (ukuranBaju == "s")
                {
                    ukuranS = 200000;
                    Console.WriteLine($" Merek Baju: {iMP} \n dengan Harga: " + ukuranS);
                }

                else if (ukuranBaju == "m")
                {
                    ukuranM = 220000;
                    Console.WriteLine($" Merek Baju: {iMP} \n dengan Harga: " + ukuranM);
                }
                else
                {
                    ukuranLain = 250000;
                    Console.WriteLine($" Merek Baju: {iMP} \n dengan Harga: " + ukuranLain);
                }
            } 
            else if (kodeBaju == 2)
            {
                if (ukuranBaju == "s")
                {
                    ukuranS = 150000;
                    Console.WriteLine($" Merek Baju: {prada} \n dengan Harga: " + ukuranS);
                }

                else if (ukuranBaju == "m")
                {
                    ukuranM = 160000;
                    Console.WriteLine($" Merek Baju: {prada} \n dengan Harga: " + ukuranM);
                }
                else
                {
                    ukuranLain = 170000;
                    Console.WriteLine($" Merek Baju: {prada} \n dengan Harga: " + ukuranLain);
                }
            }
            else if (kodeBaju == 3)
            {
                if (ukuranBaju == "s")
                {
                    ukuranS = 200000;
                    Console.WriteLine($" Merek Baju: {gucci} \n dengan Harga: " + ukuranS);
                }

                else if (ukuranBaju == "m")
                {
                    ukuranM = 200000;
                    Console.WriteLine($" Merek Baju: {gucci} \n dengan Harga: " + ukuranM);
                }
                else
                {
                    ukuranLain = 200000;
                    Console.WriteLine($" Merek Baju: {gucci} \n dengan Harga: " + ukuranLain);
                }
            }

        }

        static void tugasDay411()
        {
            Console.WriteLine("--- Soal No 11 ---");
            Console.Write("Masukkan Uang Andi: ");
            int uang = int.Parse(Console.ReadLine());

            //int[]hargaBaju = Console.ReadLine
            
            int bajuA = 35;
            int bajuB = 40;
            int bajuC = 50;
            int bajuD = 20;

            int celanaA = 40;
            int celanaB = 30;
            int celanaC = 45;
            int celanaD = 10;

            if (uang >= 75 && uang < 95)
            {
                Console.WriteLine($"Dengan Nilai Uang {uang} Kamu dapat membeli Baju A dan Celana A dengan Harga: " + (bajuA+celanaA));

            } else if (uang >= 70 && uang < 75)
            {
                Console.WriteLine($"Dengan Nilai Uang {uang} Kamu dapat membeli Baju B dan Celana B dengan Harga: " + (bajuB + celanaB));

            } else if (uang >= 95)
            {
                Console.WriteLine($"Dengan Nilai Uang {uang} Kamu dapat membeli Baju C dan Celana C dengan Harga: " + (bajuC + celanaC));

            } else if (uang >= 30 && uang < 70)
            {
                Console.WriteLine($"Dengan Nilai Uang {uang} Kamu dapat membeli Baju D dan Celana D dengan Harga: " + (bajuD + celanaD));

            } else
            {
                Console.WriteLine("Nilai Uang anda tidak sesuai Paket");
            }


        }

        static void tugasDay412()
        {
            int i, panjang;

            Console.Write("Masukkan Panjang Angka: ");
            panjang = int.Parse(Console.ReadLine());


            for (i = 1; i <= panjang; ++i)
            {
                Console.Write(i);
            }
        }
    }
}

﻿using System;
using System.Drawing;

namespace Logic02
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // ifStatement();
            // ifElse();
            ifNested();
            // ternaryIf();
            // switchCase();
            tugasA();
            //tugasB();
            Console.ReadKey();
        }
        
        static void tugasA()
        {
            Console.WriteLine("--- Tugas 1 Luas & Keliling Lingkaran ---");
            /*
            Console.Write("Masukkan Nilai Jari-Jari: ");
            double jarijari = double.Parse(Console.ReadLine());

            Console.Write("Masukkan Nilai phi: ");
            double phi = double.Parse(Console.ReadLine());
            */
            double jarijari;
            double phi;
            

            Console.Write("Masukkan Nilai Jari-Jari: ");
            jarijari = double.Parse(Console.ReadLine());
            //double jariDouble = Convert.ToDouble(jarijari);

            Console.Write("Masukkan Nilai dari Phi: ");
            phi = double.Parse(Console.ReadLine());

            double Keliling = 2 * phi * jarijari;
            double Luas = phi * jarijari * jarijari;

            Console.WriteLine($"Luas Keliling Lingkaran Adalah = {Keliling}");
            Console.WriteLine($"Luas Lingkaran Adalah = {Luas}");

        }

        static void tugasB()
        {
            Console.WriteLine("--- Tugas 2 Luas & Keliling Persegi ---");

            
            Console.Write("Masukkan Nilai Sisi: ");
            int sisi = int.Parse(Console.ReadLine());
            
            int Luas = sisi * sisi;
            int Keliling = 4 * sisi;

            Console.WriteLine($"Luas Persegi Adalah = {Luas}");
            Console.WriteLine($"Keliling Persegi Adalah = {Keliling}");
        }


        static void switchCase()
        {
            Console.WriteLine("--- Switch Case ---");
            Console.Write("Pilih Buah Kesukaan Anda ( Apel, Jeruk, Pisang) : ");
            string pilihan = Console.ReadLine().ToLower();

            switch (pilihan)
            {
                case "apel":
                    Console.WriteLine("Kamu Menyukai Buah Apel ");
                    break;
                case "jeruk":
                    Console.WriteLine("Kamu menyukai Buah Jeruk ");
                    break;
                case "pisang":
                    Console.WriteLine("Kamu menyukai Buah Pisang");
                    break;
                default:
                    Console.WriteLine("Kamu menyukai Buah Lainnya");
                    break;
            }

        }

        static void ternaryIf()
        {
            Console.WriteLine("--- Ternary If ---");

            int x, y;
            Console.Write("Masukkan Nilai X: ");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Nilai Y: " );
            y = int.Parse(Console.ReadLine());

            string hasilTernary = x > y ? "Niiali X lebih besar dari Nilai Y " :
            x < y ? "Nilai X lebih kecil dari Nilai Y " : "Nilai X sama dengan Nilai Y " ;
            Console.WriteLine(hasilTernary);


        }

        static void ifNested()
        {
            int nilai;
            int nilaiMax = 100;

            Console.Write("Masukkan Nilai Kamu : ");
            nilai = int.Parse(Console.ReadLine());

            if (nilai >= 70 && nilai <= nilaiMax)
            {
                Console.WriteLine("Kamu berhasil, Yeay");
                if (nilai == nilaiMax)
                {
                    Console.WriteLine("Kamu Sempurna");
                }
                
            } else if (nilai >= 0 && nilai < 70){
                Console.WriteLine("Kamu Gagal");
            } 
            else
            {
                Console.WriteLine("Masukkan Nilai yang benar");
            }

        }

        static void ifElse()
        {
            Console.WriteLine("--- Else Statement ---");
            int x, y;
            Console.Write("Masukkan Nilai X= ");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Nilai Y= ");
            y = int.Parse(Console.ReadLine());

            if (x > y)
            {
                Console.WriteLine("Nilai X lebih Besar dari Y");
            } 
            else if (x < y)
            {
                Console.WriteLine("Nilai Y lebih Besar dari X");
            }
            else
            {
                Console.WriteLine("Nilai X sama dengan Y");
            }
        }
        static void ifStatement()
        {
            Console.WriteLine("--- If Statement ---");
            int x, y;
            Console.Write("Masukkan Nilai X= ");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Nilai Y= ");
            y = int.Parse(Console.ReadLine());

            if (x > y)
            {
                Console.WriteLine("Nilai X lebih besar dari Y");
            }
            if (x < y)
            {
                Console.WriteLine("Nilai Y lebih besar dari X");
            }

            if (x == y)
            {
                Console.WriteLine("Nilai X sama dengan Y");
            }
            
        }
    }
}

﻿using System;
using System.Linq;

namespace PR_Day07
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //tugas01();
            //tugas02();
            //tugas03();
            //tugas04();
            //tugas05();
            //tugas06();
            //tugas06B();
            //tugas07();
            //tugas08();
            //tugas09();
            //tugas10();
            //tugas11();
            tugas12();
            Console.ReadKey();
        }

        static void tugas01()
        {

        }
        static void tugas02()
        {

        }
        static void tugas03()
        {

        }
        static void tugas04()
        {
            Console.Write("Masukkan Tanggal Mulai (dd/mm/yyyy): ");
            DateTime dateStart = DateTime.Parse(Console.ReadLine());

            Console.Write("Masukkan Hari Libur: ");
            int exam = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Tanggal Libhr: ");
            int[] holiday = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            DateTime tanggalSelesai = dateStart;

            for (int i = 0; i < exam; i++)
            {
                if (i > 0)
                {
                    tanggalSelesai = tanggalSelesai.AddDays(1);
                }
               
                if ( tanggalSelesai.DayOfWeek == DayOfWeek.Saturday)
                {
                    tanggalSelesai = tanggalSelesai.AddDays(2);
                }

                for (int j = 0; j < holiday.Length; j++)
                {
                    if (tanggalSelesai.Day == holiday[j])
                    {
                        tanggalSelesai = tanggalSelesai.AddDays(1);
                        if (tanggalSelesai.DayOfWeek == DayOfWeek.Saturday)
                        {
                            tanggalSelesai = tanggalSelesai.AddDays(2);


                        }

                    }
                }
            }

            DateTime tanggalUjian = tanggalSelesai.AddDays(1);
            if ( tanggalUjian.DayOfWeek == DayOfWeek.Saturday)
            {
                tanggalUjian = tanggalUjian.AddDays(2);
            }

            Console.WriteLine();
            Console.WriteLine("Kelas Akan Ujian pada tanggal: " + tanggalUjian.ToString("dddd, dd/MM/yyyy"));
        }
        static void tugas05()
        {
            //Console.Write("Masukkan Kalimat: ");
            //string kalimat = Console.ReadLine(); ;

            //int panjang, vokal=0, konsonan= 0;

            //foreach (char huruf in kalimat)
            //{
            //    if (huruf =)

            //}

        }
        static void tugas06()
        {
            //int i, j;
            Console.Write("Masukkan Kalimat");
            string kal = Console.ReadLine().ToLower();
            string trim = String.Concat(kal.Where(a => !Char.IsWhiteSpace(a)));

            foreach (char c in trim)
            {
                Console.Write("***" + c + "***");
                Console.WriteLine();

            }

        }

        static void tugas06B()
        {
            int i;
            Console.Write("Masukkan Kalimat: ");
            char[] kal = Console.ReadLine().ToLower().Replace(" ", "").ToCharArray();

            for (i= 0; i < kal.Length; i++)
            {
                Console.WriteLine($"***{kal[i]}***");
            }

        }
        static void tugas07()
        {
            int uang = 0;
            int totalHargaMenu = 0;
            int bisaDimakan =0;
            int totalBayar = 0;
            int sisa = 0;

            Console.Write("Masukkan Total Menu: ");
            int totalMenu = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Index Makanan Alergi: ");
            int indexMkananAlergi = int.Parse(Console.ReadLine());
            ulang:
            Console.Write("Masukkan Harga Menu: ");

            int[] hargaMenu = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
            if (hargaMenu.Length > totalMenu)
            {
                Console.WriteLine("Masukkan Uang Anda= ");
                goto ulang;
            }
            else
            {
                Console.Write("Masukkan Uang Anda");
                uang = int.Parse(Console.ReadLine());

                for (int i = 0; i < hargaMenu.Length; i++)
                {
                    totalHargaMenu += hargaMenu[i];
                }

                bisaDimakan = totalHargaMenu - hargaMenu[indexMkananAlergi];
                totalBayar = bisaDimakan / 2;
                sisa = uang - totalBayar;

                if (sisa > 0)
                {
                    Console.WriteLine($"\nAnda harus membayar= "+ totalBayar.ToString("Rp #,##"));
                    Console.WriteLine($"Sisa Uang Anda= " + sisa.ToString("Rp #,##"));
                }
                else if (sisa == 0)
                {
                    Console.WriteLine("\nUang Anda Pas tidak ada kembalian");
                }
                else
                {
                    Console.WriteLine("\nUang ANda Kurang "+ sisa.ToString("Rp #,##"));
                }
            }
        }
        static void tugas08()
        {
            Console.Write("Masukkan Panjang: ");
            int panjang = int.Parse(Console.ReadLine()); ;

            //for (int i = 1; i <= panjang; i++)
            //{
            //    for (int j = panjang - i; j >= 1; j--)
            //    {
            //        Console.Write(" ");
            //    }

            //    for (int k = 1; k <= i; k++)
            //    {
            //        Console.Write("#");
            //    }
            //    Console.Write("\n");
            //}


            for (int i = 0; i < panjang; i++)
            {
                for (int j = 0; j < panjang; j++)
                {
                    if (j < panjang - 1 - i)
                    {
                        Console.Write(" ");
                    }
                    else
                    {
                        Console.Write("#");
                    }
                    
                }

                Console.WriteLine();

            }
        }
        static void tugas09()
        {

        }
        static void tugas10()
        {
            Console.Write("--- Soal No. 10");
            Console.Write("Masukkan isi Array: ");
            int[] lilin = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            int maks = 0, banyakmaks = 0;

            for(int j = 0; j<lilin.Length; j++)
            {
                if (lilin[j] > maks)
                
                    maks = lilin[j];

                Console.WriteLine("\tNilai lilin tertinggi adalah: " + maks);
            }
            
            for( int i=0; i< lilin.Length; i++)
            {
                if (lilin[i] == maks)
                    banyakmaks++;
            }

            Console.WriteLine("\tNilai Lilin tertinggi adalah: " + maks);
            Console.WriteLine("\tJumlah Lilin tertinggi sebanyak: " + banyakmaks);
        }
        static void tugas11()
        {
            //int[] arrayIsi = { 5, 6, 7, 0, 1 };

            //Console.Write("Masukkan panjang kolo yang mau di ubah: ");
            //int angka = int.Parse(Console.ReadLine());

            //Console.Write("Masukkan JUmlah Rotasi: ");
            //int rotasi = int.Parse(Console.ReadLine());

            //int rot = 0;

            //for (int i = 0; i < rotasi; i++)
            //{
            //    for (int j = 0; j < angka.Length; j++)
            //    {
            //        if (j < 1)
            //        {
            //            rot = angka[j];
            //            angka[j] = angka[j + 1];

            //        }
            //        else if (j >= 1 && j < angka.Length - 1)
            //        {
            //            angka[j] = angka[j + 1];
            //        }
            //        else
            //        {
            //            angka[angka.Length - 1] = rot;
            //        }
            //    }
            //    Console.Write("\t");
            //    Console.Write(String.Join(" ", angka));
            //}


          
        }
        static void tugas12()
        {
            Console.Write("--- Soal No. 12");
            Console.Write("Masukkan isi Array: ");
            int[] arrayIsi = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            //int[] arrayIsi = { 2, 5, 4, 1, 3 };

            int tampung = 0;

            for (int i =0; i <arrayIsi.Length; i++)
            {
                for (int j=0; j<arrayIsi.Length-1; j++)
                {
                    if (arrayIsi[j] > arrayIsi[j + 1])
                    {

                        tampung = arrayIsi[j + 1];
                        arrayIsi[j + 1] = arrayIsi[j];
                        arrayIsi[j] = tampung;
                    }
                }
            }

            Console.Write("Hasil Sortingnya adalah: ");

            for (int k =0; k < arrayIsi.Length; k++)
            {
                
                Console.Write(arrayIsi[k] + " ");
            }
            //Console.Write(String.Join(" ", arrayIsi));
        }
    }
}

﻿using System;
using System.Drawing;

namespace PR_Day09
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //soal01();
            //soal02();
            //soal03();
            //soal03Bapak();
            Console.ReadKey();
        }
        static void soal01()
        {
            Console.WriteLine("=== SOAL Nomor 01 ===");
            Console.Write("Masukkan Total Pesanan Costumer: ");
            int input = int.Parse(Console.ReadLine());
            
            ulang:
            Console.Write("Masukkan Jarak dalam Meter: ");
            int[] arrayIsi = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            Console.Write("Masukkan KM yang dipakai dalam 1L: ");
            double bensin = double.Parse(Console.ReadLine());

            int tampung = 0;

            if (input == arrayIsi.Length)
            {
                foreach (int k in arrayIsi)
                {

                    tampung = (tampung += k);
                }

                double tampung2 = Convert.ToDouble(tampung) / 1000;

                Console.WriteLine();
                Console.WriteLine("\tTotal Jarak Tempuh Ojol: " + tampung + " M atau " + tampung2 + " KM");
                Console.Write("\tTotal Bensin yang dipakai: " + Math.Round((tampung2 / bensin), MidpointRounding.ToPositiveInfinity) + " Liter");
            }
            else
            {
                Console.WriteLine("Input Salah");
                goto ulang;
            }
           
            
            
        }
        static void soal02()
        {
            Console.WriteLine("=== SOAL Nomor 02 ===");
            Console.Write("Masukkan Banyak Kue: ");
            double input = double.Parse(Console.ReadLine());

            double terigu = 115; //gr
            double gulaPasir = 100; //gr
            double susu = 100; //ml
            double tampung = 0;

            double teriguBaru = (terigu / 15);
            double gulaBaru = (gulaPasir / 15);
            double susuBaru = (susu / 15);

            if (input > tampung)
            {
                teriguBaru*= input;
                gulaBaru *= input;
                susuBaru *= input;

                Console.WriteLine($"Banyak Bahan yang di butuhkan untuk Kue Sebanyak {input} adalah: ");
                Console.WriteLine("Terigu: " + Math.Round(teriguBaru) +"gr"+ "\nGula Pasir: " 
                    + Math.Round(gulaBaru) + "gr" + "\nSusu: "+ Math.Round(susuBaru)+"ml");
            } else
            {
                Console.WriteLine("Masukkan Jumlah Kue Yang benar!!!");
            }

        }
        static void soal03()
        {
            Console.WriteLine("=== SOAL Nomor 03 ===");
            Console.Write("Masukkan jumlah Index: ");
            int index = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai Kelipatan: ");
            int kelipatan = int.Parse(Console.ReadLine());


            int tampung = 0;
            for (int i = 0; i < index; i++)
            {
                for (int j = 0; j < index; j++)
                {
                    if (j == 0 && i >= 0)
                    {
                        tampung = kelipatan;
                        Console.Write(" " + tampung);
                    }
                    else if (j == i)
                    {
                        tampung += kelipatan * j;
                        Console.Write(" " + tampung);

                    }
                    else if (i == index - 1 && j >= 0 && j < index)
                    {
                        tampung += kelipatan;
                        Console.Write(" " + tampung);
                    }
                    else
                        Console.Write("  ");
                }
                Console.WriteLine();
            }




        }
        
        static void soal03Bapak()
        {
            Console.WriteLine("=== SOAL Nomor 03 ===");
            Console.Write("Masukkan jumlah Index: ");
            int suku = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai Kelipatan: ");
            int angka = int.Parse(Console.ReadLine());

            for (int i=0; i < suku; i++)
            {
                int x = 0;
                for (int j = 0; j < suku; j++)
                {
                    x += angka;
                    if (j == 0 || i == suku - 1 || i == j)
                    {
                        Console.Write(x + "\t");
                    }
                    else
                    {
                        Console.Write("\t");
                    }
                    
                }
                Console.WriteLine();
            }
        }
        static void soal3Cara2()
        {
            Console.WriteLine("=== SOAL Nomor 03 ===");
            Console.Write("Masukkan jumlah Index: ");
            int suku = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai Kelipatan: ");
            int angka = int.Parse(Console.ReadLine());

            
        }
    }
}
    


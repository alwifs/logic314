﻿using System;

namespace Logic04
{
    internal class Program
    {
        static void Main(string[] args)
        {
            removeString();
            //insertString();
            //replaceString();
            Console.ReadKey();
        }
        
        static void removeString()
        {
            Console.WriteLine(" --- Remove String --- ");
            Console.WriteLine("Masukkan Kalimat: ");
            string kalimat = Console.ReadLine();

            Console.WriteLine("Masukkan Parameter Remove: ");
            int param = int.Parse(Console.ReadLine());

            Console.WriteLine($"Kalimat Baru: {kalimat.Remove(param)}");
        }

        static void insertString()
        {
            Console.WriteLine(" --- Insert String --- ");
            Console.WriteLine("Masukkan Kalimat: ");
            string kalimat = Console.ReadLine();

            Console.WriteLine("Masukkan Parameter Insert: ");
            int param = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Input String:");
            string input = Console.ReadLine();


            Console.WriteLine($"Masukkan Kalimat: {kalimat.Insert(param, input)}");
        }

        static void replaceString()
        {
            Console.WriteLine(" --- Replace String --- ");
            Console.WriteLine("Masukkan Kalimat: ");
            string kalimat = Console.ReadLine();

            Console.WriteLine("Masukkan Kata Lama: ");
            string kataLama = Console.ReadLine();
            Console.Write("Masukkan Kata Baru:");
            string kataBaru = Console.ReadLine();


            Console.WriteLine($"Masukkan Kalimat: {kalimat.Replace(kataLama, kataBaru)}");
        }
    }
}

-- Task SQL Day 03 --
Create database db_sales
use db_sales

create table salesperson(
id int primary key identity (1,1),
nama varchar (50) not null,
bod date not null,
salary decimal (18,2) not null
)

insert into salesperson (nama, bod, salary) values
('Abe', '1998-11-9', 140000),
('Bob', '1978-11-9', 44000),
('Chris', '1983-11-9', 40000),
('Dan', '1980-11-9', 52000),
('Ken', '1977-11-9', 115000),
('Joe', '1990-11-9', 38000)

create table orders (
id int primary key identity(1,1),
orderdate date not null,
cust_id int null,
salesperson_id int not null,
amount int not null
)

insert into orders(orderdate, cust_id, salesperson_id, amount) values 
('2020-08-02', 4, 2, 540),
('2020-01-22', 4, 5, 1800),
('2020-07-14', 9, 1, 460),
('2020-01-29', 7, 2, 2400),
('2020-02-03', 6, 4	, 600),
('2020-03-02', 6, 4, 720),
('2020-05-06', 9, 4, 150)

--a. Informasi nama sales yang memiliki order lebih dari 1.
select distinct * from salesperson sp
join orders ord on sp.id = ord.salesperson_id where salesperson_id > 1 


select sp.nama, COUNT (ord.id)[Jumlah_Order]
from salesperson sp
join orders ord on sp.id = ord.salesperson_id where salesperson_id > 1 
group by sp.nama having COUNT(ord.id) > 1

--b. Informasi nama sales yang total amount ordernya di atas 1000.
select sal.nama, SUM(ord.amount) [Total_Amount]
from salesperson sal
join orders ord on sal.id = ord.salesperson_id
group by sal.nama
having SUM(ord.amount) > 1000

--c. Informasi nama sales, umur, gaji dan total amount order yang tahun ordernya >= 2020 dan data ditampilan berurut sesuai dengan umur (ascending).
select sal.nama,datediff(YEAR, sal.bod, GETDATE())[Umur], 
sal.salary,SUM(ord.amount)[TOtal Amount]
from salesperson sal
join orders ord on sal.id = ord.salesperson_id
where year(ord.orderdate) >= 2020
group by sal.nama,sal.salary, sal.bod
order by Umur asc
-- bisa pakai sal.bod asc

--d. Carilah rata-rata total amount masing-masig sales urutkan dari hasil yg paling besar
select sal.nama, AVG(ord.amount) as Ratarata_total
from salesperson as sal
join orders as ord on sal.id = ord.salesperson_id
group by sal.nama
order by Ratarata_total desc

--e. perusahaan akan memberikan bonus bagi sales yang berhasil memiliki order lebih dari 2 dan total order lebih dari 1000 sebanyak 30% dari salary
select sal.nama, COUNT(ord.id)[Total_ORder],
SUM(ord.amount)[Total_Amount],

from salesperson sal
join orders ord on sal.id = ord.salesperson_id
group by sal.nama, sal.salary
having COUNT(ord.id) > 2 and SUM(ord.amount) > 1000



--f. Tampilkan data sales yang belum memiliki orderan sama sekali
select sal.nama
from salesperson sal
left join orders ord on sal.id = ord.salesperson_id
where ord.salesperson_id is null

--g. Gaji sales akan dipotong jika tidak memiliki orderan,  gaji akan di potong sebanyak 2%
select sal.nama, sal.salary * 0.02[POTONGANGAJI], sal.salary[GajiKotor],
sal.salary -(sal.salary*0.02)[Gaji_Bersih]
from salesperson sal
left join orders ord on sal.id = ord.salesperson_id
where ord.salesperson_id is null


--- Tugas 2 SQL Day 03 ---

Create database db_HR
use db_HR

create table tblKaryawan (
id bigint primary key not null,
nip varchar (50) not null,
nama_depan varchar (50) not null,
nama_belakang varchar (50) not null,
agama varchar (50) not null,
tempat_lahir varchar (50) not null,
tgl_lahir date null,
alamat varchar (100) not null,
pend_terakhir varchar (50) not null,
tgl_masuk date null
)

create table tbl_divisi(
id bigint primary key identity(1,1),
kd_divisi varchar (50) not null,
nama_divisi varchar (50) not null
)

create table tbl_jabatan(
id bigint primary key identity(1,1),
kd_jabatan varchar (50) not null,
nama_jabatan varchar (50)  not null,
gaji_pokok numeric null,
tunjangan_jabatan numeric null
)

create table tbl_pekerjaan(
id bigint primary key identity(1,1),
nip varchar (50) not null,
kode_divisi varchar(50) not null,
tunjang_kinerja numeric,
kota_penempatan varchar(50)
)

alter table tbl_pekerjaan add kode_jabatan varchar (50)

insert into tblKaryawan(id, nip, nama_depan, nama_belakang, jenis_kelamin, agama, tempat_lahir, tgl_lahir, alamat, pend_terakhir, tgl_masuk) values
(1, '001', 'Hamidi', 'Samsudin', 'Pria', 'Islam', 'Sukabumi', '1977-04-21' , 'Jl. Sudirman No.12', 'S1 Teknik Mesin', '2015-12-07'),
(3, '003', 'Paul', 'Christian', 'Pria', 'Kristen', 'Ambon', '1980-05-27' , 'Jl. Veteran No.04', 'S1 Pendidikan Geografi', '2014-01-12'),
(2, '002', 'Ghandi', 'Wamida', 'Wanita', 'Islam', 'Palu', '1992-01-12' , 'Jl. Rambutan No.22', 'SMA Negeri 02 Palu', '2014-12-01')

alter table tblKaryawan add jenis_kelamin varchar (50)


insert into tbl_divisi(kd_divisi, nama_divisi) values
('GD','Gudang'),
('HRD','HRD'),
('KU','Keuangan'),
('UM','Umum')

insert into tbl_jabatan(kd_jabatan, nama_jabatan, gaji_pokok, tunjangan_jabatan) values
('MGR', 'Manager', 5500000, 1500000),
('OB', 'Office Boy', 1900000, 200000),
('ST', 'Staff', 3000000, 750000),
('WMGR', 'Wakil Manager', 4000000, 1200000)

insert into tbl_pekerjaan(nip, kode_divisi, tunjang_kinerja, kota_penempatan, kode_jabatan) values
('001', 'KU', 750000, 'Cianjur', 'ST'),
('002', 'UM', 350000, 'Sukabumi', 'OB'),
('003', 'HRD', 1500000, 'Sukabumi', 'MGR')

select * from tblKaryawan
select * from tbl_pekerjaan
select * from tbl_divisi
select * from tbl_jabatan

--No.1. Tampilkan nama lengkap, nama jabatan, tunjangan jabatan + gaji , yang gaji + tunjangan kinerja dibawah 5juta

select (kar.nama_depan+ ' '+kar.nama_belakang) as Nama_Lengkap, jab.nama_jabatan as Nama_Jabatan, 
(jab.gaji_pokok+jab.tunjangan_jabatan) as Tunjangan_Gaji
from tblKaryawan as kar
join tbl_pekerjaan as pek on kar.nip = pek.nip
join tbl_jabatan as jab on pek.kode_jabatan = jab.kd_jabatan 
where (jab.gaji_pokok+jab.tunjangan_jabatan) < 5000000

--No.2. Tampilkan nama lengkap, jabatan, nama divisi, total gaji, pajak, gaji bersih, yg gendernya pria dan penempatan kerjanya diluar sukabumi

select (kar.nama_depan+ ' '+kar.nama_belakang) as Nama_Lengkap, jab.nama_jabatan as Nama_Jabatan,
div.nama_divisi as Divisi,(jab.gaji_pokok+jab.tunjangan_jabatan+pek.tunjang_kinerja) as Total_Gaji,
((jab.gaji_pokok+jab.tunjangan_jabatan+pek.tunjang_kinerja)*0.05) as Pajak,
((jab.gaji_pokok+jab.tunjangan_jabatan+pek.tunjang_kinerja)-((jab.gaji_pokok+jab.tunjangan_jabatan+pek.tunjang_kinerja)*0.05)) as Gaji_Bersih
from tblKaryawan as kar
join tbl_pekerjaan as pek on kar.nip = pek.nip
join tbl_divisi as div on pek.kode_divisi = div.kd_divisi
join tbl_jabatan as jab on pek.kode_jabatan = jab.kd_jabatan
where kar.jenis_kelamin = 'Pria' and pek.kota_penempatan != 'Sukabumi'

--No.3. Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus (bonus=25% dari total gaji(gaji pokok+tunjangan_jabatan+tunajangan_kinerja) * 7
select kar.nip as NIP, (kar.nama_depan+ ' '+kar.nama_belakang) as Nama_Lengkap, jab.nama_jabatan as Nama_Jabatan,
div.nama_divisi as Divisi, (((jab.gaji_pokok+jab.tunjangan_jabatan+pek.tunjang_kinerja)*0.25)*7) as Bonus
from tblKaryawan as kar
join tbl_pekerjaan as pek on kar.nip = pek.nip
join tbl_divisi as div on pek.kode_divisi = div.kd_divisi
join tbl_jabatan as jab on pek.kode_jabatan = jab.kd_jabatan

--No.4. Tampilkan nama lengkap, ttal gaji, infak(5%*total gaji) yang mempunyai jabatan MGR

select (kar.nama_depan+ ' '+kar.nama_belakang) as Nama_Lengkap, (jab.gaji_pokok+jab.tunjangan_jabatan+pek.tunjang_kinerja) as Total_Gaji,
((jab.gaji_pokok+jab.tunjangan_jabatan+pek.tunjang_kinerja)*0.05) as Infaq
from tblKaryawan as kar
join tbl_pekerjaan as pek on kar.nip = pek.nip
join tbl_jabatan as jab on pek.kode_jabatan = jab.kd_jabatan
where jab.nama_jabatan = 'Manager'

--No.5. Tampilkan nama lengkap, nama jabatan, pendidikan terakhir, tunjangan pendidikan(2jt), dan total gaji(gapok+tjabatan+tpendidikan) dimana pendidikan akhirnya adalah S1

select (kar.nama_depan+ ' '+kar.nama_belakang) as Nama_Lengkap, jab.nama_jabatan as Jabatan, 2000000 as Tunjangan_Pendidikan, 
(jab.gaji_pokok+jab.tunjangan_jabatan+2000000) as Total_Gaji
from tblKaryawan as kar
join tbl_pekerjaan as pek on kar.nip = pek.nip
join tbl_jabatan as jab on pek.kode_jabatan = jab.kd_jabatan
where kar.pend_terakhir like 's1%'

--No.6. Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus
select kar.nip as NIP, (kar.nama_depan+ ' '+kar.nama_belakang) as Nama_Lengkap, jab.nama_jabatan as Nama_Jabatan,
div.nama_divisi as Divisi, ((jab.gaji_pokok+jab.tunjangan_jabatan+pek.tunjang_kinerja)*0.25) as Bonus
from tblKaryawan as kar
join tbl_pekerjaan as pek on kar.nip = pek.nip
join tbl_divisi as div on pek.kode_divisi = div.kd_divisi
join tbl_jabatan as jab on pek.kode_jabatan = jab.kd_jabatan
where jab.nama_jabatan = 'Staff' and

--No.7. Buatlah kolom nip pada table karyawan sebagai kolom unique
create unique index index_nip on tblKaryawan(nip)
drop index unique_index_nip on tblKaryawan

--No.8. Buatlah kolom nip pada table karyawan sebagai index
create index index_nip on tblKaryawan(nip)

--No.9. Tampilkan nama lengkap, nama belakangnya diubah menjadi huruf capital dengan kondisi nama belakang di awali dengan huruf W
select (kar.nama_depan + ' ' + kar.nama_belakang) as Nama_Lengkap, upper(kar.nama_belakang) as Nama_Belakang
from tblKaryawan as kar
join tbl_pekerjaan as pek on kar.nip = pek.nip
where kar.nama_belakang like 'W%'

--No.10.  Perusahaan akan memberikan bonus sebanyak 10% dari total gaji bagi karyawan yg sudah join di peruashaan diatas sama dengan  8 tahun
--Tampilkan nip, nama lengkap, jabatan, nama divisi, total gaji , bonus, lama bekerja\
--(total gaji = Gaji_pokok+Tunjangan_jabatan+Tunjangan_kinerja )


--SQL Day 02--

-- CAST
select CAST(10 as decimal (10,4)) -- convert dec
select CAST('10' as int) -- string ke int
select CAST('2023-03-16' as date) as datetime -- convert string ke date
select GETDATE() as Hari_Ini -- mendapatkan waktu hari ini
select day(GETDATE()), month(GETDATE()), year(GETDATE())

-- COnvert
select convert(decimal(18,4), 10)
select CONVERT (int, '10')
select CONVERT (int, 10.65)
select CONVERT (datetime, '2023-03-16')

--date add
select DATEADD (year,2,getdate()), DATEADD(MONTH,3,GETDATE()),DATEADD(DAY,5,GETDATE())

--date Diff
select DATEDIFF(DAY, '2023-03-16', '2023-03-25'), DATEDIFF(MONTH, '2023-03-16', '2024-06-16') as DAtediff

-- SUbQuery
insert into [dbo].[mahasiswa] -- menambahkan nilai sebanyak data di tabel mahasiswa/ mengkali data dengan data
select nama,alamat,email,panjang from mahasiswa 

delete from mahasiswa where id > 9 -- menghapus  data lebih dari id > 9

select nama,alamat,email,panjang from mahasiswa
where panjang = (select max(panjang) from mahasiswa) -- select didalam select untuk menampilkan nilai tertinggi

insert into [dbo].[mahasiswa] -- 
select nama,alamat,email,panjang from mahasiswa

-- view sql
create view viwMahasiswa as select * from mahasiswa 

drop view viwMahasiswa --drop view

--creat database index
create index index_nama on mahasiswa (nama) --membuat indext
create index index_alamat_email on mahasiswa (alamat,email) --membuat index dari 2 colom

select * from mahasiswa where nama='Alwi Fadli'

select * from mahasiswa where alamat= 'Riau'
select * from mahasiswa where email= 'alwifadlis@gmail.com'

-- unique index
create unique index Uniqueindex_alamat2 on mahasiswa(panjang)

drop index index_alamat_email on mahasiswa -- drop index
drop index Uniqueindex_alamat on mahasiswa -- drop unique index

-- primary key & foreignkey
select * from mahasiswa
select * from biodata

alter table mahasiswa
add constraint pk_panjang primary key (panjang) -- contoh cara add primary key

-- drop primary key
alter table mahasiswa drop constraint PK__mahasisw__3213E83FA60991A5
alter table mahasiswa drop constraint pk_panjang

alter table mahasiswa
add constraint pk_id_panjang primary key (id, panjang) -- primary key 2 buah panjang dan id

-- add unique constraint
alter table mahasiswa add constraint unique_panjang unique(panjang)

-- drop unique constraint
alter table mahasiswa drop constraint unique_panjang

--- update data di unique constraint
update mahasiswa set panjang = 50 where id =2 -- error, karenatidak boleh nilai sama karena sudah di set unique

--create databasse
CREATE DATABASE db_kampus
CREATE DATABASE db_kampus2

--memanggil db
use db_kampus

--- Create table
create table mahasiswa (
id bigint primary key identity(1,1),
nama varchar (50) not null,
alamat varchar (50) not null,
email varchar (250) null
)
create table dosen (
id bigint primary key identity(1,1),
nama varchar (50) not null,
alamat varchar (50) not null,
email varchar (250) null
)

--alter table
alter table dosen add description varchar (255) -- alter add table description
alter table dosen drop column [description] -- alter drop table description
alter table dosen alter column email varchar(100) -- alter update table description
alter view [NamaViewLama] to [NamaViewBaru]

--drop syntax
drop database db_kampus2 --drop database
drop table mahasiswa2 --drop table
drop view [NamaView]


--create view (view adalah table bayangan, bisa hasil dari penggabungan table)
create view vMahasiswa
as --querynya diisini
select id,nama,alamat,email from mahasiswa

-- select view
select * from viewmahasiswa --Hasill view
drop view viewMahasiswa

--DML
--Inser Data
insert into mahasiswa(nama, alamat, email) 
values ('Alwi Fadli', 'Riau', 'alwifadlis@gmail.com')
insert into mahasiswa(nama, alamat, email) 
values ('Siregar', 'Riau', 'siregar@gmail.com')
insert into mahasiswa(nama, alamat, email) values 
('Toni Agustina', 'Garut', 'toniagustina@gmail.com'),
('Agustina Dewi', 'Riau', 'Agustinadewi@gmail.com'),
('Siti Kholilah', 'Riau', 'siti@gmail.com')

--Select Data
select id,nama,alamat,email from mahasiswa -- query detail
select * from mahasiswa -- alternatif query lebih cepat

-- Update Data
update mahasiswa set nama='Ardo Siregar', alamat='Duri' where id=2
update mahasiswa set nama='Isni Dwitini', alamat='Garut' where nama='Toni Agustina'
update mahasiswa set nama='Astika Pujiati', alamat='Garut' where id=3

--Delete Data
delete from mahasiswa where id=7
delete from mahasiswa where nama='Siti Kholilah'

--Join Data
--create table join
create table biodata (
id bigint primary key identity(1,1),
mahasiswa_id bigint not null,
dob datetime not null,
kota varchar (100) not null
)

-- join table
select * from mahasiswa as mhs join biodata as bio on mhs.id = bio.id 
where mhs.nama = 'Siti Kholilah'

select * from mahasiswa as mhs left join biodata as bio on mhs.id = bio.id 
where mhs.nama = 'Siti Kholilah' -- pakai left join



--isi data biodata
insert into biodata(mahasiswa_id, dob, kota) values 
(1, '1996-03-31', 'Jakarta Selatan'),
(2, '2005-03-05', 'Riau'),
(3, '1999-05-23', 'Jakarta'),
(4, '1998-04-15', 'Jakarta'),
(5, '1999-08-31', 'Riau'),
(6, '2003-05-03', 'Riau')

select * from biodata

select mhs.id, mhs.nama, bio.kota, bio.kota, bio.dob
from mahasiswa as mhs
join biodata as bio on mhs.id = bio.mahasiswa_id
where mhs.nama = 'Siti Kholilah' -- detail join

alter table biodata alter column dob date not null -- ubah tipe data dob dari datetime menjadi date

--Sql Server Where (AND) -- test lagi nanti (belum solve)
select mhs.id, mhs.nama, bio.kota, bio.kota, bio.dob
from mahasiswa as mhs
join biodata as bio on mhs.id = bio.mahasiswa_id
where mhs.nama = 'Siti Kholilah' and bio.kota='Jakarta' -- hasil false karena siti bukan dijakarta logika and

select mhs.id, mhs.nama, bio.kota, bio.kota, bio.dob
from mahasiswa as mhs
join biodata as bio on mhs.id = bio.mahasiswa_id
where mhs.nama = 'Siti Kholilah' or bio.kota='Jakarta' -- hasil false karena siti bukan dijakarta logika and

--order by
select * from mahasiswa mhs
join biodata bio on mhs.id = bio.mahasiswa_id
order by mhs.id asc, mhs.nama desc

--select top
select top 2 * from mahasiswa order by nama asc -- memunculkan 2 list paling atas sesuai asc
select top 2 * from mahasiswa order by nama desc -- memunculkan 2 list paling bawah sesuai dsc

--between
select * from mahasiswa where id between 1 and 3 -- between menampilkan daftar dari 1 sampai 3
select * from mahasiswa where id >= 1 and id <= 4 -- cara selain between menampilkan daftar dari 1 sampai 4

--like fungsi
select * from mahasiswa where nama like 'a%' -- sorting huruf depan a
select * from mahasiswa where nama like '%i' -- sorting huruf belakang i
select * from mahasiswa where nama like '%fadli%' -- sorting like kata fadli ditengah
select * from mahasiswa where nama like '-a%'
select * from mahasiswa where nama like '__n%'
select * from mahasiswa where nama like 'i__%'
select *  from mahasiswa where nama like 'i%i'

--group by
select nama, alamat from mahasiswa group by nama, alamat
select sum(id), nama from mahasiswa group by nama

--having by
select count(id), nama from mahasiswa group by nama having count(id) > 0

--distinct contoh
select distinct nama from mahasiswa -- distinct menghapus data dublikat
select * from mahasiswa

-- Substring SQL
select SUBSTRING('SQL Tutorial ', 1, 3) as ekstakString -- hasil SQL dari huruf 1-3

-- Char index
select CHARINDEX('t', 'Costumer') as judul --huruf t ada di nilai ke 4 hasil dari charindex

select DATALENGTH('akumau istirahat') -- menghitung jumlah huruf kalimat

-- Case 
select * from mahasiswa
alter table mahasiswa add panjang smallint 
alter table mahasiswa alter column panjang smallint not null

update mahasiswa set panjang= 50 where id=1
update mahasiswa set panjang= 86 where id=2
update mahasiswa set panjang= 117 where id=3
update mahasiswa set panjang= 120 where id=4
update mahasiswa set panjang= 125 where id=5
update mahasiswa set panjang= 160 where id=6
update mahasiswa set panjang= 110 where id=8

update mahasiswa set email='astikapujiati@gmail.com'  where id=3
update mahasiswa set email='isnidwiani@gmail.com'  where id=4

select *

alter table mahasiswa add panjang2 smallint not null -- tidak mau

-- Case
select id,nama,alamat,
	case when panjang > 50 then 'Pendek'
	when panjang <= 100 then 'Sedang'
	else panjang > 100 then 'Tinggi'
	end as tinggiBadan
from mahasiswa
-- Case

-- Concat
select CONCAT('SQL ', 'Is ', 'Fun')
select 'SQL' + 'Sangat' + 'Seru'

-- Operator aritmatika
create table penjualan (
id bigint primary key identity(1,1),
nama varchar (50) not null,
harga bigint not null,
hargaJumlah bigint not null
)

select * from penjualan
alter table penjualan alter column hargaJumlah bigint 

insert into penjualan(nama, harga) values
--('Indomie', 1500),
('Close-Up', 3500),
('Pepsoden', 3000),
('Brush Formula', 2500),
('Roti Manis', 1000),
('Gula', 3500),
('Sarden', 4500),
('Rokok Sempurna', 11000),
('Rokok 234', 11000)

alter table penjualan drop column hargaJumlah

select *, harga*100 as [totalHarga] from penjualan


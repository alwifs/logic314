create database db_penerbit
use db_penerbit

create table tblPengarang (
id bigint primary key identity (1,1),
kdPengarang varchar (7),
nama varchar (30),
alamat varchar (80),
kota varchar (15),
jKelamin varchar (1)
)
create table tblGaji(
id bigint primary key identity (1,1),
kdPengarang varchar (7),
nama varchar (30),
gaji decimal (18) 
)

insert into tblPengarang(kdPengarang, nama, alamat, kota, jKelamin) values
('P000', 'Ashadi', 'Jl.Beo 25', 'Yogya', 'P'),
('P0002', 'Rian', 'Jl. Solo 123', 'Yogya', 'P'),
('P0003', 'Suwadi', 'Jl. Semangka 13', 'Bandung', 'P'),
('P0004', 'Siti', 'Jl. Durian 15', 'Solo', 'W'),
('P0005', 'Amir', 'Jl. Gajah 33', 'Kudus', 'P'),
('P0006', 'Suparman', 'Jl. Harimau 25', 'Jakarta', 'P'),
('P0007', 'Jaja', 'Singa 7', 'Bandung', 'P'),
('P0008', 'Saman', 'Naga 12', 'Yogya', 'P'),
('P0009', 'Anwar', 'Tidar 6A', 'Magelang', 'P'),
('P0010', 'Fatmawati', 'Jl. Renjana 4', 'Bogor', 'W')

insert into tblGaji(kdPengarang, nama, gaji) values
('P0002', 'Rian', 600000),
('P0005', 'Amir', 700000),
('P0004', 'Siti', 500000),
('P0003', 'Suwadi', 1000000),
('P0010', 'Fatmawati', 600000),
('P0008', 'Saman', 750000)

select * from tblPengarang
select * from tblGaji

-- No. 1. Hitung dan tampilkan jumlah pengarang dari table tblPengarang.
select COUNT(id) nama from tblPengarang

-- No. 2. Hitunglah berapa jumlah Pengarang Wanita dan Pria
select Count(id) as jumlahKelamin, jKelamin from tblPengarang group by jKelamin

-- No. 3. Tampilkan record kota dan jumlah kotanya dari table tblPengarang.
select Count(id), kota from tblPengarang group by kota

-- No. 4. Tampilkan record kota diatas 1 kota dari table tblPengarang.
select count(kota)Kota, kota from tblPengarang group by kota having count(id) > 1

-- No. 5. Tampilkan Kd_Pengarang yang terbesar dan terkecil dari table tblPengarang.
select MAX (kdPengarang) as KDPengarangTerbesar, MIN (kdPengarang) as KDPengarangTerendah from tblPengarang
--cara lain
select
(select top 1 kdPengarang from tblPengarang order by kdPengarang desc) as kode_tertinggi,
(select top 1 kdPengarang from tblPengarang order by kdPengarang asc) as kode_terendah

-- No. 6. Tampilkan gaji tertinggi dan terendah.
select MAX(gaji) as gajiTertinggi, MIN(gaji) as gajiTerendah from tblGaji

-- No. 7. Tampilkan gaji diatas 600.000.
select gaji from tblGaji where gaji > 600000

-- No. 8. Tampilkan jumlah gaji.
select sum(gaji) as JumlahTotalGaji from tblGaji 

alter table tblGaji alter column gaji decimal (18,4)

-- No. 9. Tampilkan jumlah gaji berdasarkan Kota
select kota, sum(gaji) as jumlah_gaji 
from tblGaji as gaji
join tblPengarang as ab  on gaji.kdPengarang = ab.kdPengarang  group by kota
-- pakai right join nampilkan null
select kota, sum(gaji) as jumlah_gaji 
from tblGaji as gaji
right join tblPengarang as ab  on gaji.kdPengarang = ab.kdPengarang  group by kota

-- No. 10. Tampilkan seluruh record pengarang antara P0003-P0006 dari tabel pengarang.
select * from tblPengarang where kdPengarang between 'P0003' and 'P0006'
select * from tblPengarang where kdPengarang >= 'P0003' and kdPengarang <= 'P0006'

	
-- No. 11. Tampilkan seluruh data yogya, solo, dan magelang dari tabel pengarang.
select * from tblPengarang where kota = 'yogya' or kota = 'solo' or kota = 'magelang'
select * from tblPengarang where kota in ('yogya','solo','magelang')

-- No. 12. Tampilkan seluruh data yang bukan yogya dari tabel pengarang.
select * from tblPengarang where kota <> 'yogya' -- pakai <>
select * from tblPengarang where kota != 'yogya' -- pakai !=
select * from tblPengarang where not kota='yogya' -- pakai not

-- No. 13. Tampilkan seluruh data pengarang yang nama (terpisah): a. dimulai dengan huruf [A], b. berakhiran [i], c. huruf ketiganya [a], d. tidak berakhiran [n]
select * from tblPengarang where nama like 'a%' --A 
select * from tblPengarang where nama like '%i' -- B
select * from tblPengarang where kdPengarang not like '%4' and nama like '%i' -- masukan bapak

select * from tblPengarang where nama like '__a%' -- C
select * from tblPengarang where nama not like '%n' --- D

-- No. 14. Tampilkan seluruh data table tblPengarang dan tblGaji dengan Kd_Pengarang yang sama
select ab.kdPengarang, gaji.Gaji
from tblGaji as gaji join tblPengarang as ab  on gaji.kdPengarang = ab.kdPengarang  group by ab.kdPengarang, gaji.Gaji

-- No. 15. Tampilan kota yang memiliki gaji dibawah 1.000.000
select a.gaji, b.kota from tblGaji as a 
join tblPengarang as b on b.kdPengarang = a.kdPengarang 
where a.gaji < 1000000

-- No. 16. Ubah panjang dari tipe kelamin menjadi 10
alter table tblPengarang alter column jKelamin varchar(10)

-- No. 17. Tambahkan kolom [Gelar] dengan tipe Varchar (12) pada tabel tblPengarang
alter table tblPengarang add gelar varchar(12)
alter table tblPengarang drop column gelar

-- No. 18. Ubah alamat dan kota dari Rian di table tblPengarang menjadi, Jl. Cendrawasih 65 dan Pekanbaru
update tblPengarang	 set alamat='Jl. Cendrawasih 65' where id=2
update tblPengarang	 set kota='Pekanbaru' where id=2

select * from tblPengarang


-- No. 19. Buatlah view untuk attribute Kd_Pengarang, Nama, Kota, Gaji dengan nama vwPengarang
create view vwPengarang as select tblPengarang.id, tblPengarang.kdPengarang, 
tblPengarang.nama, tblPengarang.kota, tblGaji.gaji from tblPengarang, tblGaji where tblGaji.kdPengarang = tblPengarang.kdPengarang



select * from vwPengarang
select * from vwPengarang2
drop view vwPengarang2


-- trik pluggin sql
sp_columns 'tblPengarang'
sp_help 'tblPengarang'
	--- TUgas SQL DAY 02 ---
--create database
create database entertainer
use entertainer
-- create table
create table artis (
kd_artis varchar (100) primary key,
nm_artis varchar (100),
jkelamin varchar (100),
bayaran bigint,
award int,
negara varchar(100)
)
 

create table film (
kd_film varchar (10) primary key,
nm_film varchar (55),
genre varchar (55),
artis varchar (55),
produser varchar (55),
pendapatan bigint,
nominasi int
)

create table produser (
kd_produser varchar (100) primary key,
nm_produser varchar (50),
international varchar (50)
)

create table negara (
kd_negara varchar (100) primary key,
nm_negara varchar (100)
)

create table genre (
kd_genre varchar (50) primary key,
nm_genre varchar (50)
)

alter table artis alter column bayaran bigint
alter table film alter column pendapatan bigint


select * from artis
select * from film 
select * from produser
select * from negara
select * from genre

-- insert data
insert into artis(kd_artis, nm_artis, jkelamin, bayaran, award, negara) values
('A001', 'ROBERT DOWNEY JR', 'PRIA', 3000000000, 2, 'AS'),
('A002', 'ANGELINE JOLIE', 'WANITA', 700000000, 1, 'AS'),
('A003', 'JACKIE CHAN', 'PRIA', 200000000, 7, 'HK'),
('A004', 'JOE TASLIM', 'PRIA', 350000000, 1, 'ID'),
('A005', 'CHELSEA ISLAN', 'WANITA', 300000000, 0, 'ID')

insert into film(kd_film, nm_film, genre, artis, produser, pendapatan, nominasi) values
('F001', 'IRON MAN', 'G001', 'A001', 'PD01', 2000000000, 3),
('F002', 'IRON MAN', 'G001', 'A001', 'PD01', 1800000000, 2),
('F003', 'IRON MAN', 'G001', 'A001', 'PD01', 1200000000, 0),
('F004', 'AVENGER CIVIL WAR', 'G001', 'A001', 'PD01', 2000000000, 1),
('F005', 'SPIDERMAN HOME COMING', 'G001', 'A001', 'PD01', 1300000000, 0),
('F006', 'THE RAID', 'G001', 'A004', 'PD03', 800000000, 5),
('F007', 'FAST & FURIOUS', 'G001', 'A004', 'PD05', 830000000, 2),
('F008', 'HABIBI DAN AINUN', 'G004', 'A005', 'PD03', 670000000, 4),
('F009', 'POLICE STORY', 'G001', 'A003', 'PD02', 700000000, 3),
('F0010', 'POLICE STORY 2', 'G001', 'A003', 'PD02', 710000000, 1),
('F0011', 'POLICE STORY 3', 'G001', 'A003', 'PD02', 615000000, 0),
('F0012', 'RUSH HOUR', 'G003', 'A003', 'PD05', 695000000, 2),
('F0013', 'KUNGFU PANDA', 'G003', 'A003', 'PD05', 923000000, 5)

update film set nm_film = 'IRON MAN 2' where kd_film = 'F002' 
update film set nm_film = 'IRON MAN 3' where kd_film = 'F003'

drop table film

insert into produser(kd_produser, nm_produser, international) values
('PD01', 'MARVEL', 'YA'),
('PD02', 'HONGKONG CINEMA', 'YA'),
('PD03', 'RAPI FILM', 'TIDAK'),
('PD04', 'PARKIT', 'TIDAK'),
('PD05', 'PARAMOUNT PICTURE', 'YA')

insert into negara(kd_negara, nm_negara) values
('AS', 'AMERIKA SERIKAT'),
('HK', 'HONGKONG'),
('ID', 'INDONESIA'),
('IN', 'INDIA')

insert into genre(kd_genre, nm_genre) values
('G001', 'ACTION'),
('G002', 'HORRO'),
('G003', 'COMEDY'),
('G004', 'DRAMA'),
('G005', 'THRILLER'),
('G006', 'FICTION')


-- NO.1. Menampilkan jumlah pendapatan produser marvel secara keseluruhan

select produser.nm_produser, sum(film.pendapatan) as pendapatan from film
join produser on film.produser = produser.kd_produser where produser.nm_produser = 'Marvel' group by produser.nm_produser

-- NO.2. Menampilkan nama film dan nominasi yang tidak mendapatkan nominasi
select nominasi, nm_film from film where nominasi = 0

-- NO.3. Menampilkan nama film yang huruf depannya 'p'
select film.nm_film from film where nm_film like 'p%'

-- NO.4. Menampilkan nama film yang huruf terakhir 'y'
select film.nm_film from film where nm_film like '%y'

-- NO.5. Menampilkan nama film yang mengandung huruf 'd'
select film.nm_film from film where nm_film like '%d%'

-- NO.6. Menampilkan nama film dan artis
create view vwartisfilm as select nm_film, nm_artis from film, artis where artis = kd_artis

select nm_film, nm_artis from film, artis where artis = kd_artis

select * from vwartisfilm
drop view vwartisfilm

-- NO.7. Menampilkan nama film yang artisnya berasal dari negara hongkong
select film.nm_film, artis.negara from film join artis on film.artis = artis.kd_artis where negara = 'HK'

-- NO.8. Menampilkan nama film yang artisnya bukan berasal dari negara yang mengandung huruf 'o'
select film.nm_film, negara.nm_negara from film join artis on film.artis = artis.kd_artis 
join negara on negara.kd_negara = artis.negara where negara.nm_negara not like '%O%'

-- NO.9. Menampilkan nama artis yang tidak pernah bermain film
select	artis.nm_artis from artis left join film on film.artis = artis.kd_artis where film.nm_film is null

select	artis.nm_artis from film right join artis on film.artis = artis.kd_artis where film.nm_film is null

select	artis.nm_artis from artis left join film on film.artis = artis.kd_artis 
group by artis.nm_artis having COUNT(film.kd_film) <1

-- NO.10. Menampilkan nama artis yang bermain film dengan genre drama
select artis.nm_artis, genre.nm_genre from film join genre on film.genre = genre.kd_genre 
join artis on artis.kd_artis = film.artis where genre.nm_genre = 'drama'

-- NO.11. Menampilkan nama artis yang bermain film dengan genre Action
select artis.nm_artis, genre.nm_genre from film join genre on film.genre = genre.kd_genre
join artis on artis.kd_artis = film.artis where genre.nm_genre = 'action'
group by artis.nm_artis, genre.nm_genre
-- dengan distinct
select distinct artis.nm_artis, genre.nm_genre from film join genre on film.genre = genre.kd_genre
join artis on artis.kd_artis = film.artis where genre.nm_genre = 'action'

-- NO.12. Menampilkan data negara dengan jumlah filmnya
select negara.kd_negara, negara.nm_negara, COUNT(film.artis) jumlah_film from negara 
left join artis on negara.kd_negara = artis.negara full join
film on film.artis = artis.kd_artis group by negara.kd_negara, negara.nm_negara


-- NO.13. Menampilkan nama film yang skala internasional
select film.nm_film from film join produser on film.produser = produser.kd_produser where international like 'y%' 

-- NO.14. Menampilkan jumlah film dari masing2 produser
select produser.nm_produser, COUNT(film.produser) from produser full join film on produser.kd_produser = film.produser 
group by produser.nm_produser order by produser.nm_produser asc
